from __future__ import annotations
import io
from typing import TYPE_CHECKING

import pandas as pd
from PIL import Image

from src.config import settings
from src.helpers.direct_links import DropboxDirectLinks
from src.helpers.tilda import TildaCsvFile, TildaCsvFileUploader


if TYPE_CHECKING:
    from pathlib import Path


def upload_csvfile_to_tilda(csvfile_path: str | Path) -> None:
    uploader = TildaCsvFileUploader(
        filepath=csvfile_path,
        email=settings.tilda_email,
        password=settings.tilda_password,
        project_id=settings.tilda_project_id,
        selenium_timeout=settings.selenium_timeout,
        file_uploading_timeout=settings.file_uploading_timeout,
    )
    uploader.upload_file()


def get_tilda_csvfile(
    odata_parts: pd.DataFrame, persistence_df: pd.DataFrame | None
) -> TildaCsvFile:
    csvfile = TildaCsvFile()

    csvfile = csvfile.add_skus(skus=get_skus(odata_parts))
    csvfile = csvfile.add_external_ids(
        external_ids=get_external_ids(odata_parts)
    )
    csvfile = csvfile.add_prices(prices=get_prices(odata_parts))
    csvfile = csvfile.add_descriptions(
        descriptions=get_descriptions(odata_parts)
    )
    csvfile = csvfile.add_titles(titles=get_titles(odata_parts))
    csvfile = csvfile.add_brands(brands=get_brands(odata_parts))
    csvfile = csvfile.add_categories(categories=get_categories(odata_parts))
    csvfile = csvfile.add_characteristic(
        name="Категория", values=get_categories_characteristic(odata_parts)
    )
    return csvfile.add_photos(photos=get_photos(odata_parts, persistence_df))


def get_skus(odata_parts: pd.DataFrame) -> pd.Series[str]:
    return odata_parts["Артикул"]


def get_external_ids(odata_parts: pd.DataFrame) -> pd.Series[str]:
    return odata_parts["Ref_Key"]


def get_prices(odata_parts: pd.DataFrame) -> pd.Series[int]:
    return odata_parts["Цена"].map(
        lambda price: int(price) if not pd.isna(price) else price
    )


def get_descriptions(odata_parts: pd.DataFrame) -> pd.Series[str]:
    return odata_parts["КоличествоBalance"].map(  # noqa: RUF001
        lambda quantity: "В наличии" if quantity > 0 else "На заказ"  # noqa: RUF001
    )


def get_titles(odata_parts: pd.DataFrame) -> pd.Series[str]:
    titles = odata_parts["НаименованиеПолное"].str.replace(",", ", ")
    titles += " " + odata_parts["Артикул"].map(lambda sku: f" [арт. {sku}]")
    return titles.str.replace("\n", " ")


def get_brands(odata_parts: pd.DataFrame) -> pd.Series[str]:
    return odata_parts["Folders"].map(
        lambda folders: next(
            folder.name for folder in folders if folder.nesting_level == 1
        )
    )


def get_categories(odata_parts: pd.DataFrame) -> list[pd.Series[str] | str]:
    brands = get_brands(odata_parts)
    brands_category = brands.map(lambda brand: f"Запчасти/{brand}")
    return ["Запчасти/Каталог", brands_category]


def get_categories_characteristic(odata_parts: pd.DataFrame) -> pd.Series[str]:
    characteristic = odata_parts["Folders"].map(
        lambda folders: next(
            (folder.name for folder in folders if folder.nesting_level == 2),  # noqa: PLR2004
            None,
        )
    )
    return characteristic.map(
        lambda text: text[0].upper() + text[1:] if text is not None else None
    )


def get_photos(
    odata_parts: pd.DataFrame, persistence_df: pd.DataFrame | None
) -> pd.Series[str]:
    dropbox = get_dropbox()

    default_photo_link = dropbox.get_direct_link(
        content=settings.default_photo_path.read_bytes(),
        filename=settings.default_photo_path.name,
    )

    add_photos_column_from_persistence_df(
        odata_parts=odata_parts,
        persistence_df=persistence_df,
        default_photo_link=default_photo_link,
    )

    return odata_parts.apply(
        lambda row: get_photo_direct_link(
            dropbox=dropbox,
            sku=row["Артикул"],
            photo_link=row["Photo"],
            default_photo_link=default_photo_link,
        ),
        axis=1,
    )


def add_photos_column_from_persistence_df(
    odata_parts: pd.DataFrame,
    persistence_df: pd.DataFrame | None,
    default_photo_link: str,
) -> None:
    if persistence_df is None:
        odata_parts["Photo"] = default_photo_link
    else:
        merged = odata_parts.merge(
            persistence_df,
            left_on="Ref_Key",
            right_on="External ID",
            how="outer",
            indicator=True,
        )
        merged = merged[merged["_merge"] != "right_only"]
        odata_parts["Photo"] = merged["Photo"].fillna(default_photo_link)


def get_dropbox() -> DropboxDirectLinks:
    return DropboxDirectLinks(
        refresh_token=settings.dropbox_refresh_token,
        app_key=settings.dropbox_app_key,
        app_secret=settings.dropbox_app_secret,
        folder_path=settings.dropbox_folder_path,
    )


def get_photo_direct_link(
    dropbox: DropboxDirectLinks,
    sku: str,
    photo_link: str,
    default_photo_link: str,
) -> str:
    if photo_link != default_photo_link:
        return photo_link

    photo_path = find_photo_by_sku(sku=sku)
    if photo_path is None:
        return photo_link

    resized_photo = resize_photo(
        photo_path=photo_path, width=settings.photo_width
    )

    return dropbox.get_direct_link(
        content=resized_photo,
        filename=photo_path.name,
    )


def find_photo_by_sku(sku: str) -> Path | None:
    for path in settings.photos_path.iterdir():
        is_file = path.is_file()
        is_jpg = path.suffix == ".jpg"
        is_matched_with_sku = sku + ".jpg" == path.name
        if is_file and is_jpg and is_matched_with_sku:
            return path
    return None


def resize_photo(photo_path: Path, width: int) -> bytes:
    with Image.open(photo_path) as photo:
        base_width, base_height = photo.size
        height = int(base_height * width / base_width)
        resized_photo = photo.resize((width, height))
        photo_bytes = io.BytesIO()
        resized_photo.save(photo_bytes, "jpeg")
        return photo_bytes.getvalue()
