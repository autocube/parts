import pandas as pd

from src.config import settings
from src.helpers import odata


def get_odata_parts() -> pd.DataFrame:
    odata_client = get_odata_client()

    price_types = get_price_types(odata_client)
    prices = get_prices(odata_client)
    products = get_products(odata_client)
    stocks = get_stocks(odata_client)

    retail_price_ref_key = get_retail_price_ref_key(price_types)
    retail_prices = get_retail_prices(prices, retail_price_ref_key)

    products = merge_retail_prices(products, retail_prices)
    products = merge_stocks(products, stocks)

    products = add_folders_column(products)

    return filter_parts(products).reset_index(drop=True)


def get_odata_client() -> odata.ODataClient:
    return odata.ODataClient(
        url=settings.odata_url,
        username=settings.odata_username,
        password=settings.odata_password,
    )


def get_price_types(odata_client: odata.ODataClient) -> pd.DataFrame:
    return odata_client.get_entities(
        name="Catalog_ТипыЦен", select=["Ref_Key", "Description"]
    )


def get_prices(odata_client: odata.ODataClient) -> pd.DataFrame:
    return odata_client.get_entities(
        name="InformationRegister_Цены_RecordType/SliceLast()",
        select=["Номенклатура_Key", "ТипЦен_Key", "Цена"],
    )


def get_products(odata_client: odata.ODataClient) -> pd.DataFrame:
    return odata_client.get_entities(
        name="Catalog_Номенклатура",
        select=[
            "Ref_Key",
            "Parent_Key",
            "IsFolder",
            "Description",
            "НаименованиеПолное",
            "Артикул",
        ],
    )


def get_stocks(odata_client: odata.ODataClient) -> pd.DataFrame:
    return odata_client.get_entities(
        name="AccumulationRegister_ОстаткиТоваровКомпании/Balance()",
        select=["Номенклатура_Key", "КоличествоBalance"],  # noqa: RUF001
    )


def get_retail_price_ref_key(price_types: pd.DataFrame) -> str:
    retail_price_mask = price_types["Description"] == "Розничная цена"
    retail_price_ref_key = price_types[retail_price_mask]["Ref_Key"].iloc[0]

    if not isinstance(retail_price_ref_key, str):
        raise TypeError

    return retail_price_ref_key


def get_retail_prices(
    prices: pd.DataFrame, retail_price_ref_key: str
) -> pd.DataFrame:
    return prices[prices["ТипЦен_Key"] == retail_price_ref_key]


def merge_retail_prices(
    products: pd.DataFrame, retail_prices: pd.DataFrame
) -> pd.DataFrame:
    return products.merge(
        retail_prices,
        how="outer",
        left_on="Ref_Key",
        right_on="Номенклатура_Key",
    )


def merge_stocks(products: pd.DataFrame, stocks: pd.DataFrame) -> pd.DataFrame:
    return products.merge(
        stocks,
        how="outer",
        left_on="Ref_Key",
        right_on="Номенклатура_Key",
    )


def add_folders_column(products: pd.DataFrame) -> pd.DataFrame:
    return odata.add_folders_column(
        products_df=products,
        init_parent_key="00000000-0000-0000-0000-000000000000",
        key_column="Ref_Key",
        parent_key_column="Parent_Key",
        is_folder_column="IsFolder",
        folder_name_column="Description",
        folders_column="Folders",
    )


def filter_parts(products: pd.DataFrame) -> pd.DataFrame:
    products = products[~products["Артикул"].isna()]
    folder_names_series = products["Folders"].map(
        lambda folders: [folder.name for folder in folders]
    )
    part_mask = folder_names_series.map(
        lambda folder_names: "Запасные части" in folder_names
        and ("FOTON" in folder_names or "ASHOK" in folder_names)
    )
    return products[part_mask]
