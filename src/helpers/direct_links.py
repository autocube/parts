import dropbox
from dropbox.exceptions import ApiError
from loguru import logger


class DropboxDirectLinks:
    def __init__(
        self,
        refresh_token: str,
        app_key: str,
        app_secret: str,
        folder_path: str = "",
    ) -> None:
        self._dropbox = dropbox.Dropbox(
            oauth2_refresh_token=refresh_token,
            app_key=app_key,
            app_secret=app_secret,
        )
        self._folder_path = folder_path

    def get_direct_link(self, content: bytes, filename: str) -> str:
        dropbox_filepath = f"{self._folder_path}/{filename}"

        with logger.contextualize(dropbox_filepath=dropbox_filepath):
            self._upload_file(
                content=content, dropbox_filepath=dropbox_filepath
            )
            self._create_shared_link(dropbox_filepath=dropbox_filepath)

        result = self._dropbox.sharing_list_shared_links(
            path=dropbox_filepath, direct_only=True
        )

        link = result.links[0].url.replace(
            "www.dropbox.com", "dl.dropboxusercontent.com"
        )

        if not isinstance(link, str):
            raise TypeError

        return link

    def _upload_file(self, content: bytes, dropbox_filepath: str) -> None:
        self._dropbox.files_upload(content, path=dropbox_filepath)
        logger.info("The file was successfully uploaded to Dropbox.")

    def _create_shared_link(self, dropbox_filepath: str) -> None:
        try:
            self._dropbox.sharing_create_shared_link_with_settings(
                path=dropbox_filepath
            )
            logger.info(
                "A direct link to the file in Dropbox was successfully "
                "created.",
            )
        except ApiError:
            logger.info(
                "The direct link to the file in Dropbox was not created as it "
                "was created earlier.",
            )
