from __future__ import annotations
import csv
import hashlib
from copy import deepcopy
from pathlib import Path
from typing import Any, Sequence

import numpy as np
import pandas as pd
from loguru import logger
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class TildaCsvFile:
    def __init__(self) -> None:
        self._products_df = pd.DataFrame()

    def add_skus(self, skus: pd.Series[str]) -> TildaCsvFile:
        return self._add_column("SKU", skus)

    def add_external_ids(self, external_ids: pd.Series[str]) -> TildaCsvFile:
        return self._add_column("External ID", external_ids)

    def add_prices(
        self,
        prices: pd.Series[str]
        | pd.Series[int]
        | pd.Series[float]
        | str
        | float,
    ) -> TildaCsvFile:
        return self._add_column("Price", prices)

    def add_descriptions(
        self, descriptions: pd.Series[str] | str
    ) -> TildaCsvFile:
        return self._add_column("Description", descriptions)

    def add_titles(self, titles: pd.Series[str] | str) -> TildaCsvFile:
        return self._add_column("Title", titles)

    def add_brands(self, brands: pd.Series[str] | str) -> TildaCsvFile:
        return self._add_column("Brand", brands)

    def add_photos(self, photos: pd.Series[str] | str) -> TildaCsvFile:
        return self._add_column("Photo", photos)

    def add_categories(
        self, categories: Sequence[pd.Series[str] | str]
    ) -> TildaCsvFile:
        new_tilda_csv_file = self._add_column("Category", categories[0])
        for i in range(1, len(categories)):
            new_tilda_csv_file._products_df["Category"] += ";" + categories[i]  # noqa: SLF001
        return new_tilda_csv_file

    def add_characteristic(
        self,
        name: str,
        values: pd.Series[str]
        | pd.Series[int]
        | pd.Series[float]
        | str
        | float,
    ) -> TildaCsvFile:
        return self._add_column(f"Characteristics:{name}", values)

    def exclude_rows(self, persistence_df: pd.DataFrame) -> TildaCsvFile:
        new_df = self._products_df.fillna(np.NaN)
        new_df["row_hash"] = new_df.apply(_hash_series, axis=1)

        old_df = persistence_df.fillna(np.NaN)
        old_df["row_hash"] = old_df.apply(_hash_series, axis=1)

        merged = new_df.merge(
            old_df,
            on="row_hash",
            how="outer",
            suffixes=("", "_y"),
            indicator=True,
        )
        left_only = merged[merged["_merge"] == "left_only"]
        new_tilda_csv_file = TildaCsvFile()
        new_tilda_csv_file._products_df = deepcopy(  # noqa: SLF001
            left_only[self._products_df.columns]
        )

        return new_tilda_csv_file

    def save_to(self, filepath: str | Path) -> None:
        self._products_df.to_csv(
            Path(filepath), index=False, quoting=csv.QUOTE_ALL
        )

    def _add_column(
        self,
        column: str,
        values: pd.Series[str]
        | pd.Series[int]
        | pd.Series[float]
        | str
        | float,
    ) -> TildaCsvFile:
        new_tilda_csv_file = TildaCsvFile()
        new_tilda_csv_file._products_df = deepcopy(self._products_df)  # noqa: SLF001
        new_tilda_csv_file._products_df[column] = deepcopy(values)  # noqa: SLF001
        return new_tilda_csv_file


class TildaCsvFileUploader:
    def __init__(  # noqa: PLR0913
        self,
        filepath: Path | str,
        email: str,
        password: str,
        project_id: str,
        selenium_timeout: int,
        file_uploading_timeout: int,
    ) -> None:
        self._email = email
        self._password = password
        self._project_id = project_id
        self._filepath = Path(filepath)
        self._selenium_timeout = selenium_timeout
        self._file_uploading_timeout = file_uploading_timeout

        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--window-size=1920,1080")
        self._driver = webdriver.Chrome(options=chrome_options)

        self._driver.implicitly_wait(self._selenium_timeout)

    def upload_file(self) -> None:
        if self._file_is_empty():
            logger.info(
                "File upload to Tilda was skipped because the file is empty."
            )
        else:
            self._login_to_tilda()
            self._upload_file()

    def _file_is_empty(self) -> bool:
        return len(pd.read_csv(self._filepath)) == 0

    def _login_to_tilda(self) -> None:
        self._driver.get("https://tilda.cc/login")

        email_input = self._driver.find_element(By.ID, "email")
        email_input.send_keys(self._email)

        password_input = self._driver.find_element(By.ID, "password")
        password_input.send_keys(self._password)

        password_input.send_keys(Keys.ENTER)

        wait = WebDriverWait(self._driver, self._selenium_timeout)

        try:
            wait.until(
                expected_conditions.url_to_be("https://tilda.cc/projects/")
            )
            logger.info("Login to Tilda's account was successfully completed.")
        except TimeoutException:
            logger.error(
                "An error occurred while logging into Tilda's account."
            )
            raise

    def _upload_file(self) -> None:
        self._driver.get(
            f"https://store.tilda.cc/store/?projectid={self._project_id}"
        )

        self._driver.execute_script("tstore_start_import('csv')")  # type: ignore[no-untyped-call]

        select_file_button = self._driver.find_element(
            By.CLASS_NAME, "js-import-load-file-btn"
        )
        select_file_button.click()

        hidden_file_input = self._driver.find_element(
            By.CSS_SELECTOR, "input[type='file']"
        )
        hidden_file_input.send_keys(str(self._filepath.absolute()))

        self._driver.execute_script(  # type: ignore[no-untyped-call]
            "document.querySelector('.js-import-load-data').classList"
            ".remove('disabled')"
        )

        submit_file_button = self._driver.find_element(
            By.CLASS_NAME, "js-import-load-data"
        )
        submit_file_button.click()

        replace_images_input = self._driver.find_element(
            By.CSS_SELECTOR,
            ".js-import-repalceimages + .tstore-checkbox__indicator",
        )
        replace_images_input.click()

        submit_import_options_button = self._driver.find_element(
            By.CLASS_NAME, "btn_importcsv_proccess"
        )
        submit_import_options_button.click()

        wait = WebDriverWait(self._driver, self._file_uploading_timeout)
        results_element_locator = (
            By.CLASS_NAME,
            "t-store__import__results",
        )

        try:
            wait.until(
                expected_conditions.visibility_of_element_located(
                    results_element_locator
                )
            )
            logger.info("The file was successfully uploaded to Tilda.")
        except TimeoutException:
            logger.error(
                "An error occurred while uploading the file to Tilda."
            )
            raise


def _hash_series(series: pd.Series[Any]) -> str:
    values = series.to_numpy().tolist()
    encoded_values = str(values).encode()
    return hashlib.sha256(encoded_values).hexdigest()
