from __future__ import annotations
from copy import copy
from dataclasses import dataclass
from typing import Iterable

import pandas as pd
import requests
from loguru import logger
from requests.auth import HTTPBasicAuth


class ODataClient:
    def __init__(self, url: str, username: str, password: str) -> None:
        if not url.endswith("/"):
            url += "/"
        self._odata_url = url
        self._auth = HTTPBasicAuth(
            username=username.encode("utf-8"),
            password=password.encode("utf-8"),
        )

    def get_entities(
        self, name: str, select: Iterable[str] | None = None, timeout: int = 10
    ) -> pd.DataFrame:
        params = {"$format": "json"}
        if select is not None:
            params["$select"] = ",".join(select)

        response = requests.get(
            self._odata_url + name,
            auth=self._auth,
            params=params,
            timeout=timeout,
        )
        entities_df = pd.DataFrame(response.json()["value"])

        logger.info(
            "The entities were successfully loaded via OData.",
            entity_name=name,
        )

        return entities_df


@dataclass(slots=True)
class Folder:
    name: str
    nesting_level: int


def add_folders_column(  # noqa: PLR0913
    products_df: pd.DataFrame,
    init_parent_key: str,
    key_column: str,
    parent_key_column: str,
    is_folder_column: str,
    folder_name_column: str,
    folders_column: str,
) -> pd.DataFrame:
    products_df = products_df.copy(deep=True)
    products_df[folders_column] = None

    def add_folders_recursively(
        parent_key: str,
        nesting_level: int = 0,
        parent_folders: list[Folder] | None = None,
    ) -> None:
        subset = products_df[products_df[parent_key_column] == parent_key]

        if parent_folders is None:
            parent_folders = []

        for index in subset.index:
            products_df.at[index, folders_column] = copy(parent_folders)  # noqa: PD008

            if products_df.loc[index, is_folder_column]:
                folder = Folder(
                    name=products_df.loc[index, folder_name_column],
                    nesting_level=nesting_level,
                )
                add_folders_recursively(
                    parent_key=products_df.loc[index, key_column],
                    nesting_level=nesting_level + 1,
                    parent_folders=[*parent_folders, folder],
                )

    add_folders_recursively(parent_key=init_parent_key)

    return products_df
