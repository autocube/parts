import sys

import pandas as pd
import sentry_sdk
from loguru import logger

from src.config import settings
from src.odata_parts import get_odata_parts
from src.tilda_csvfile import get_tilda_csvfile, upload_csvfile_to_tilda


def setup_logging() -> None:
    logger.remove(0)
    logging_format = "{time} | {level: <8} | {message} Details: {extra}"
    logger.add(sys.stdout, format=logging_format, level="DEBUG")
    logger.add(
        settings.logfile_path,
        format=logging_format,
        level="INFO",
        rotation="25 MB",
    )


def setup_sentry() -> None:
    sentry_sdk.init(
        dsn=settings.sentry_dsn,
        traces_sample_rate=1.0,
        profiles_sample_rate=1.0,
    )


def main() -> None:
    setup_logging()

    setup_sentry()

    odata_parts = get_odata_parts()

    persistence_df = None
    if settings.persistent_csvfile_path.is_file():
        persistence_df = pd.read_csv(settings.persistent_csvfile_path)

    tilda_csvfile = get_tilda_csvfile(
        odata_parts=odata_parts, persistence_df=persistence_df
    )

    csvfile_to_upload = tilda_csvfile
    if persistence_df is not None:
        csvfile_to_upload = tilda_csvfile.exclude_rows(
            persistence_df=persistence_df
        )
    csvfile_to_upload.save_to(filepath=settings.csvfile_to_upload_path)
    upload_csvfile_to_tilda(settings.csvfile_to_upload_path)
    tilda_csvfile.save_to(filepath=settings.persistent_csvfile_path)

    sentry_sdk.capture_message("The upload was completed successfully")


if __name__ == "__main__":
    with logger.catch(reraise=True):
        main()
