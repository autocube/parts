from pathlib import Path

from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    model_config = SettingsConfigDict(
        env_file=".env", env_file_encoding="utf-8"
    )

    sentry_dsn: str
    logfile_path: Path
    odata_url: str
    odata_username: str
    odata_password: str
    tilda_email: str
    tilda_password: str
    tilda_project_id: str
    selenium_timeout: int
    file_uploading_timeout: int
    persistent_csvfile_path: Path
    csvfile_to_upload_path: Path
    dropbox_refresh_token: str
    dropbox_app_key: str
    dropbox_app_secret: str
    dropbox_folder_path: str
    default_photo_path: Path
    photos_path: Path
    photo_width: int


settings = Settings()
