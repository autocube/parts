# Выгрузка запчастей на сайт

Инструмент позволяет настроить автоматическую выгрузку запчастей из информационной базы 1С по
протоколу [OData](https://www.odata.org/documentation/odata-version-3-0/odata-version-3-0-core-protocol/) на сайт,
созданный с помощью конструктора сайтов Tilda Publishing.

## Технологический стек

Хотя поддержка синхронизации по
протоколу [CommerceML](https://v8.1c.ru/tekhnologii/obmen-dannymi-i-integratsiya/standarty-i-formaty/standarty-commerceml/commerceml-2/)
существует, используемая в Автокуб конфигурация 1С Альфа-Авто:Автосалон+Автосервис+Автозапчасти не предполагает
реализацию этого протокола.

В связи с этим появилась необходимость создать инструмент, который с помощью REST-подобного
протокола [OData](https://www.odata.org/documentation/odata-version-3-0/odata-version-3-0-core-protocol/) получает
данные всех номенклатур, выделяет из них необходимые для выгрузки запчасти, формирует CSV-файл
по [определенным правилам](https://help-ru.tilda.cc/online-store-payments/import-export) и загружает его в Tilda с
помощью Selenium, так как Tilda для подобных целей не предоставляет API.

Изображения запчастей хранятся в общей папке, в которую сотрудники периодически добавляют новые фотографии. Tilda
требует, чтобы фотографии были загружены в облачное хранилище, которое позволяет получать прямые ссылки на загруженные
файлы. Среди всех облачных хранилищ таким является только Dropbox, API которого и был использован в проекте. Кроме того,
для оптимизации изображения уменьшаются до указанной ширины с помощью Pillow.

Таким образом, в проекте используются следующие технологии:

- Pandas
- Selenium
- Pillow
- Sentry
- Dropbox API

## Переменные окружения

Чтобы запустить проект, необходимо добавить следующие переменные в `.env`-файл:

- `SENTRY_DSN`
- `LOGFILE_PATH`
- `ODATA_URL`
- `ODATA_USERNAME`
- `ODATA_PASSWORD`
- `TILDA_EMAIL`
- `TILDA_PASSWORD`
- `TILDA_PROJECT_ID`
- `SELENIUM_TIMEOUT`
- `FILE_UPLOADING_TIMEOUT`
- `DEFAULT_PHOTO_PATH`
- `PERSISTENT_CSVFILE_PATH`
- `CSVFILE_TO_UPLOAD_PATH`
- `PHOTOS_PATH`
- `PHOTO_WIDTH`
- `DROPBOX_REFRESH_TOKEN`
- `DROPBOX_APP_KEY`
- `DROPBOX_APP_SECRET`
- `DROPBOX_FOLDER_PATH`

Для получения refresh токена для Dropbox API можно
воспользоваться [этой статьей](https://www.codemzy.com/blog/dropbox-long-lived-access-refresh-token).

## Запуск

Инструмент разворачивается ручным образом в сети организации на сервере с операционной системой Windows Server. Далее c
помощью планировщика заданий настраивается ежедневный запуск bat-файла со следующим содержанием:

```shell
setlocal
cd <project_path>
set "PYTHONPATH=%PYTHONPATH%;%cd%\src"
poetry install
poetry run python .\src\main.py
endlocal
exit /B 0
```

## Авторы

- [@nktrnv](https://gitlab.com/nktrnv)


